FROM openresty/openresty:1.13.6.2-2-bionic

RUN /usr/local/openresty/luajit/bin/luarocks install lua-cjson
RUN /usr/local/openresty/luajit/bin/luarocks install lua-resty-string
RUN /usr/local/openresty/luajit/bin/luarocks install lua-resty-mlcache

ADD lua /etc/nginx/lua
ADD nginx.conf /usr/local/openresty/nginx/conf/nginx.conf
ADD verifyApplicationNumber.conf /etc/nginx/verifyApplicationNumber.conf

EXPOSE 5000

ENTRYPOINT \
    sed -i 's~CONFIG_HOST~'${CONFIGURATION_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~SECURITY_HOST~'${SECURITY_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~ASSETMANAGER_HOST~'${ASSETMANAGER_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~ACTIVITY_LOG_SERVICE_HOST~'${ACTIVITY_LOG_SERVICE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~QUESTIONNAIRE_HOST~'${QUESTIONNAIRE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~QUESTIONNAIRE_FILTERS_HOST~'${QUESTIONNAIRE_FILTERS_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~USER_PROFILE_HOST~'${USER_PROFILE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~REMINDER_SERVICE_HOST~'${REMINDER_SERVICE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~ASSIGNMENT_SERVICE_HOST~'${ASSIGNMENT_SERVICE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~DOCITT_CAPACITY_CONNECT_HOST~'${DOCITT_CAPACITY_CONNECT_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~REQUIRED_CONDITION_SERVICE_HOST~'${REQUIRED_CONDITION_SERVICE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~DOCUMENT_MANAGER_SERVICE_HOST~'${DOCUMENT_MANAGER_SERVICE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~CONFIGURATION_SERVICE_HOST~'${CONFIGURATION_SERVICE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~CALENDER_EVENT_SERVICE_HOST~'${CALENDER_EVENT_SERVICE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~APPLICATION_SERVICE_HOST~'${APPLICATION_SERVICE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~APPLICATION_FILTER_SERVICE_HOST~'${APPLICATION_FILTER_SERVICE_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    # sed -i 's~THEME_HOST~'${THEME_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    sed -i 's~REDIS_HOST~'${REDIS_HOST}'~g' /usr/local/openresty/nginx/conf/nginx.conf && \
    nginx -g 'daemon off;'